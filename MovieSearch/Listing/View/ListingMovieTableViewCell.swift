//
//  ListingMovieTableViewCell.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit
import Kingfisher

final class ListingMovieTableViewCell: UITableViewCell {
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var popularityLabel: UILabel!
}

extension ListingMovieTableViewCell: ConfigurableCell {
    func configure(with model: Any) {
        guard let model = model as? ListingMovieCellViewModel else {
            return
        }

        if let imageURL = model.posterURL {
            posterImageView.kf.setImage(with: imageURL)
        } else {
            posterImageView.image = nil
        }

        titleLabel.text = model.title
        popularityLabel.text = model.popularity
    }
}
