//
//  ListingViewController.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit
import PKHUD

final class ListingViewController: UIViewController {
    var dataStore: ListingDataStore? = nil
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let dataStore = dataStore {
            update(with: dataStore)
        }

        setupTableView(tableView, with: dataStore)
    }

    /// Setup the tableview with given datastore
    func setupTableView(_ tableView: UITableView, with dataStore: ListingDataStore?) {
        dataStore?.manage(tableView: tableView)

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)

        tableView.refreshControl = refreshControl
    }

    /// Update the table view with given datastore
    public func update(with dataStore: ListingDataStore) {
        dataStore.delegate = self
        self.dataStore = dataStore
        refreshData()
    }

    /// Refresh the datastore by triggering fetch
    func refreshData() {
        dataStore?.reset()
        dataStore?.fetchNextPage()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let sendingTableViewCell = sender as? UITableViewCell else { return }
        guard let selectedIndexPath = tableView.indexPath(for: sendingTableViewCell) else { return }
        guard let model = dataStore?.rows[selectedIndexPath.row] as? ListingMovieCellViewModel else { return }

        guard segue.identifier == "list.to.detail" else { return }
        guard let detailVC = segue.destination as? DetailViewController else { return }

        detailVC.networkClient = dataStore?.apiClient
        detailVC.configure(with: DetailViewModel.from(prefetched: model))
    }
}

extension ListingViewController: ListingDataStoreDelegate {
    func didBeginFetching() {
        HUD.show(.progress)
    }

    func didEndFetching() {
        HUD.hide()
        tableView?.refreshControl?.endRefreshing()
    }
}

extension ListingViewController: StoryboardInstantiable {
    static var storyboardName = "Main"
    static var identifier = "ListingViewController"
    static var bundle: Bundle? = nil
}
