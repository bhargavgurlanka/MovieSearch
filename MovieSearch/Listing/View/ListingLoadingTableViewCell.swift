//
//  ListingLoadingTableViewCell.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit

final class ListingLoadingTableViewCell: UITableViewCell {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicator.startAnimating()
    }
}

extension ListingLoadingTableViewCell: ConfigurableCell {
    func configure(with model: Any) {
        /// No need to configure anything
    }
}
