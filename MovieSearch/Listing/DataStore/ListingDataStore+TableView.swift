//
//  ListingDataStore+TableView.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit

extension ListingDataStore: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = rows[indexPath.row]

        guard let cell = (tableView.dequeueReusableCell(withIdentifier: cellViewModel.cellIdentifier, for: indexPath) as? ConfigurableCell) else {
            fatalError("Cannot deque cell")
        }

        cell.configure(with: cellViewModel)

        return cell as! UITableViewCell
    }
}

extension ListingDataStore: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cellViewModel = rows[indexPath.row]
        if cellViewModel is ListingLoadingCellViewModel {
            fetchNextPage()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rows[indexPath.row].rowHeight
    }
}
