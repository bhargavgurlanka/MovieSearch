//
//  ListingDataStore.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit
import Result

/// Datastore providing pages of movies from themoviedb.org
final class ListingDataStore: NSObject {
    let apiClient: APIClient
    let params: Params
    var delegate: ListingDataStoreDelegate? = nil
    private(set) var tableView: UITableView?
    private(set) var rows: [ListingCellViewModel]
    private(set) var lastPageNumber: Int?
    private(set) var totalPagesCount: Int?
    private let FIRST_PAGE_INDEX = 1
    var hasMorePages: Bool {
        guard let lastPageNumber = lastPageNumber,
              let totalPagesCount = totalPagesCount else {
            return false
        }

        return lastPageNumber < totalPagesCount
    }

    init(apiClient: APIClient, params: Params) {
        self.apiClient = apiClient
        self.tableView = nil
        self.rows = []
        self.lastPageNumber = nil
        self.totalPagesCount = nil
        self.params = params
    }

    public func manage(tableView: UITableView) {
        tableView.dataSource = self
        tableView.delegate   = self
        self.tableView = tableView
    }

    /// Start fetching from first page from consecutive requests
    public func reset() {
        let indexPaths = (0..<rows.count).map({ IndexPath(row: $0, section: 0) })

        self.rows = []
        tableView?.deleteRows(at: indexPaths, with: .automatic)

        self.lastPageNumber = nil
        self.totalPagesCount = nil
    }

    /// Fetch the next page of movies
    public func fetchNextPage() {
        let nextPageNumber = (lastPageNumber.map { $0 + 1 } ) ?? FIRST_PAGE_INDEX

        delegate?.didBeginFetching()

        print("Fetching page: \(nextPageNumber)")

        apiClient.fetchMovies(
            params: params,
            page: nextPageNumber,
            completion: {[weak self] result in
                switch result {
                case let .success(response):
                    self?.appendRows(from: response)
                default: break
                }

                self?.delegate?.didEndFetching()
            }
        )
    }

    /// Checks if a page is already fetched
    private func isPageAlreadyFetched(_ page: Int) -> Bool {
        return lastPageNumber.map({ page <= $0 }) ?? false
    }

    /// Append rows to datastore + table view
    private func appendRows(from response: FetchMoviesListResponse) {

        /// Prevent duplicate items
        guard !isPageAlreadyFetched(response.page) else {
            return
        }

        let newRows: [ListingCellViewModel] = response.results.map(ListingMovieCellViewModel.init)

        lastPageNumber = response.page
        totalPagesCount = response.totalPages

        if let last = rows.last, last is ListingLoadingCellViewModel {
            rows.removeLast()
        }

        rows.append(contentsOf: newRows)

        if hasMorePages {
            rows.append(ListingLoadingCellViewModel())
        }

        tableView?.reloadData()
    }
}
