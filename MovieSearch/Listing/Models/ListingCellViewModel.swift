//
//  ListingCellViewModel.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit

protocol ListingCellViewModel {
    var cellIdentifier: String { get }
    var rowHeight: CGFloat { get }
}
