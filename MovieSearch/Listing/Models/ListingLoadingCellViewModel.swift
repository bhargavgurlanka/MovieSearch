//
//  ListingLoadingCellViewModel.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit

struct ListingLoadingCellViewModel: ListingCellViewModel {
    let cellIdentifier = "ListingLoadingTableViewCell"
    let rowHeight: CGFloat = 50
}
