//
//  ListingMovieCellViewModel.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit

/// View model for each movie in listing screen
struct ListingMovieCellViewModel: ListingCellViewModel {
    let id: Int
    let posterURL: URL?
    let title: String
    let overview: String
    let popularity: String
    let cellIdentifier = "ListingMovieTableViewCell"
    let rowHeight: CGFloat = 150

    init(_ movie: FetchMoviesListResponse.Movie) {
        self.id = movie.id
        self.posterURL = movie.posterURL
        self.title = TitleFormatter.format(title: movie.title, originalTitle: movie.originalTitle)
        self.overview = movie.overview
        self.popularity = String(format: "%0.2f", movie.popularity)
    }
}
