//
//  DetailViewModel.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation

struct DetailViewModel {
    let id: Int
    let posterURL: URL?
    let title: String
    let overview: String
    let genres: String
    let runtime: String
    let language: String

    init(
        id: Int,
        posterURL: URL?,
        title: String,
        originalTitle: String,
        overview: String,
        genres: [String],
        runtime: Int?,
        language: String
    ) {
        self.id = id
        self.posterURL = posterURL
        self.title = TitleFormatter.format(title: title, originalTitle: originalTitle)
        self.overview = "Overview: \(overview.isEmpty ? "-" : overview)"
        self.genres = "Genres: \(genres.isEmpty ? "-" : genres.joined(separator: ", "))"
        self.runtime = "Runtime: \(runtime.map({ String($0) + " minutes" }) ?? "-")"
        self.language = "Language: \(language.isEmpty ? "-" : language)"
    }

    init(
        id: Int,
        posterURL: URL?,
        title: String,
        overview: String,
        genres: String,
        runtime: Int?,
        language: String
    ) {
        self.id = id
        self.posterURL = posterURL
        self.title = title
        self.overview = "Overview: \(overview.isEmpty ? "-" : overview)"
        self.genres = "Genres: \(genres.isEmpty ? "-" : genres)"
        self.runtime = "Runtime: \(runtime.map({ String($0) + " minutes" }) ?? "-")"
        self.language = "Language: \(language.isEmpty ? "-" : language)"
    }


    static func from(prefetched viewModel: ListingMovieCellViewModel) -> DetailViewModel {
        return DetailViewModel(
            id: viewModel.id,
            posterURL: viewModel.posterURL,
            title: viewModel.title,
            overview: viewModel.overview,
            genres: "",
            runtime: nil,
            language: ""
        )
    }

    static func from(response: FetchMovieInfoResponse) -> DetailViewModel {
        return DetailViewModel(
            id: response.id,
            posterURL: response.posterURL,
            title: response.title,
            originalTitle: response.originalTitle,
            overview: response.overview,
            genres: response.genres,
            runtime: response.runtime,
            language: response.originalLanguage
        )
    }
}
