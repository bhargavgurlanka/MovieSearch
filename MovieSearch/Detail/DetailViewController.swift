//
//  DetailViewController.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit
import SafariServices
import Kingfisher
import PKHUD

class DetailViewController: UIViewController, SFSafariViewControllerDelegate {
    var viewModel: DetailViewModel? = nil
    var networkClient: APIClient? = nil

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        configure(with: viewModel)
        fetchMovieInfo()
    }

    func configure(with model: DetailViewModel?) {
        guard let viewModel = model else { return }

        posterImageView?.kf.setImage(with: viewModel.posterURL)
        titleLabel?.text = viewModel.title
        overviewLabel?.text = viewModel.overview
        runtimeLabel?.text = viewModel.runtime
        languageLabel?.text = viewModel.language
        genreLabel?.text = viewModel.genres

        self.viewModel = viewModel
    }

    func fetchMovieInfo() {
        guard let viewModel = viewModel,
              let networkClient = networkClient else {
            return 
        }

        HUD.show(.progress)
        networkClient.fetchMovieInfo(id: viewModel.id) { [weak self] result in
            switch result {
            case let .success(response):
                HUD.hide()
                self?.configure(with: DetailViewModel.from(response: response))
            case .failure:
                HUD.flash(.error, delay: 2.0)
            }
        }
    }

    @IBAction func openBookingView(_ sender: UIButton) {
        let vc = SFSafariViewController(url: URL(string: "http://www.cathaycineplexes.com.sg/")!)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }

    //MARK:- SFSafariViewControllerDelegate
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
