//
//  FetchMovieInfoResponse.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation

struct FetchMovieInfoResponse {
    let id: Int
    let posterURL: URL?
    let title: String
    let originalTitle: String
    let overview: String
    let originalLanguage: String
    let runtime: Int?
    let genres: [String]

    static func from(jsonData: Data, imagesBaseURL: URL) -> FetchMovieInfoResponse? {
        guard let mayBeJSONDict = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any],
            let jsonDict = mayBeJSONDict else {
                return nil
        }

        guard let id = jsonDict["id"] as? Int else { return nil }
        guard let posterPath = jsonDict["poster_path"] as? String else { return nil }
        guard let title = jsonDict["title"] as? String else { return nil }
        let overview = (jsonDict["overview"] as? String) ?? ""
        guard let originalTitle = jsonDict["original_title"] as? String else { return nil }
        guard let originalLanguage = jsonDict["original_language"] as? String else { return nil }
        let runtime = jsonDict["runtime"] as? Int
        guard let genres = jsonDict["genres"] as? [[String: Any]] else { return nil }

        return FetchMovieInfoResponse(
            id: id,
            posterURL: imagesBaseURL.appendingPathComponent(posterPath),
            title: title,
            originalTitle: originalTitle,
            overview: overview,
            originalLanguage: originalLanguage,
            runtime: runtime,
            genres: (genres.flatMap({ $0["name"] }) as? [String]) ?? []
        )
    }
}
