//
//  FetchMoviesListResponse.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation

/// Response of discover/movie endpoint
struct FetchMoviesListResponse {
    let page: Int
    let results: [Movie]
    let totalResults: Int
    let totalPages: Int

    struct Movie {
        let id: Int
        let posterURL: URL?
        let title: String
        let overview: String
        let originalTitle: String
        let popularity: Double

        static func from(jsonDict: [String: Any], imagesBaseURL: URL) -> Movie? {
            guard let id = jsonDict["id"] as? Int else {
                return nil
            }

            let posterURL = (jsonDict["poster_path"] as? String).map({ imagesBaseURL.appendingPathComponent($0) })

            let title = (jsonDict["title"] as? String) ?? ""
            let overview = (jsonDict["overview"] as? String) ?? ""
            let originalTitle = (jsonDict["original_title"] as? String) ?? ""
            let popularity = (jsonDict["popularity"] as? Double) ?? 0

            return Movie(id: id, posterURL: posterURL, title: title, overview: overview, originalTitle: originalTitle, popularity: popularity)
        }
    }

    static func from(jsonData: Data, imagesBaseURL: URL) -> FetchMoviesListResponse? {
        guard let mayBeJSONDict = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any],
              let jsonDict = mayBeJSONDict else {
            return nil
        }

        guard let page = jsonDict["page"] as? Int else {
            return nil
        }

        guard let totalResults = jsonDict["total_results"] as? Int else {
            return nil
        }

        guard let totalPages = jsonDict["total_pages"] as? Int else {
            return nil
        }

        guard let moviesDictArray = jsonDict["results"] as? [[String: Any]] else {
            return nil
        }

        let results = moviesDictArray.flatMap({ Movie.from(jsonDict:$0, imagesBaseURL: imagesBaseURL) })

        return FetchMoviesListResponse(page: page, results: results, totalResults: totalResults, totalPages: totalPages)
    }
}
