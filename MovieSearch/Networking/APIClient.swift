//
//  APIClient.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation
import Result

/// Contract for fetching movies info through themoviedb.org
protocol APIClient {

    /// Fetches a given page of movies matching provided params (search criteria)
    func fetchMovies(
        params: Params,
        page: Int,
        completion: @escaping ((Result<FetchMoviesListResponse, APIError>) -> Void)
    )

    /// Fetched info about movie with given id
    func fetchMovieInfo(id: Int, completion: @escaping ((Result<FetchMovieInfoResponse, APIError>) -> Void))
}
