//
//  MovieDBAPIClient.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation
import Result

/// Implementation of APIClient for themoviedb.org
final class MovieDBAPIClient: APIClient {
    let session: URLSession
    let config: APIClientConfig

    init(config: APIClientConfig) {
        self.session = URLSession(configuration: .default)
        self.config = config
    }

    func fetchMovies(params: Params, page: Int, completion: @escaping ((Result<FetchMoviesListResponse, APIError>) -> Void)) {
        guard let url = url(base: config.baseURL, path: config.discoverMoviePath, params: params, page: page) else {
            return completion(.failure(.unknownError))
        }

        print("Hitting: \(url)")

        let imagesBaseURL = config.imagesBaseURL

        let dataTask = session.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                DispatchQueue.main.async { completion(.failure(.unknownError)) }
                return
            }

            guard let parsedResponse = FetchMoviesListResponse.from(jsonData: data, imagesBaseURL: imagesBaseURL) else {
                DispatchQueue.main.async { completion(.failure(.unknownError)) }
                return
            }

            DispatchQueue.main.async { completion(.success(parsedResponse)) }
        }

        dataTask.resume()
    }

    func fetchMovieInfo(id: Int, completion: @escaping ((Result<FetchMovieInfoResponse, APIError>) -> Void)) {
        let url = config.baseURL.appendingPathComponent(config.movieInfoPath).appendingPathComponent(String(id))
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        components.queryItems = [
            URLQueryItem(name: "api_key", value: config.apiKey)
        ]

        let finalURL = components.url!
        print("Hitting: \(finalURL)")

        let dataTask = session.dataTask(with: finalURL) { [weak self] (data, response, error) in
            guard let data = data, let strongSelf = self else {
                DispatchQueue.main.async { completion(.failure(.unknownError)) }
                return
            }

            guard let parsedResponse = FetchMovieInfoResponse.from(jsonData: data, imagesBaseURL: strongSelf.config.imagesBaseURL) else {
                DispatchQueue.main.async { completion(.failure(.unknownError)) }
                return
            }

            DispatchQueue.main.async { completion(.success(parsedResponse)) }
        }

        dataTask.resume()
    }

    private func url(base: URL, path: String, params: Params, page: Int) -> URL? {
        var components = URLComponents()
        components.scheme = base.scheme
        components.host = base.host
        components.path = path
        components.queryItems = [
            URLQueryItem(name: "api_key", value: config.apiKey),
            URLQueryItem(name: "page", value: String(page))
        ] + params.asQueryItems()

        return components.url
    }
}
