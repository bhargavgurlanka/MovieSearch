//
//  Params.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation

/// Search criteria while making fetch requests
struct Params {
    var releasedBefore: Date?
    var sortBy: Sort?
    let dateFormatter: DateFormatter

    init() {
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
    }

    enum Sort {
        case ascending(Field)
        case descending(Field)

        enum Field: String {
            case releaseDate = "release_date"
        }

        func asQueryItem() -> URLQueryItem {
            switch self {
            case let .ascending(field):
                return URLQueryItem(name: "sort_by", value: "\(field.rawValue).asc")
            case let .descending(field):
                return URLQueryItem(name: "sort_by", value: "\(field.rawValue).desc")
            }
        }
    }

    func asQueryItems() -> [URLQueryItem] {
        var queryItems = [URLQueryItem]()
        if let releasedBefore = releasedBefore {
            queryItems.append(URLQueryItem(name: "primary_release_date.lte", value: dateFormatter.string(from: releasedBefore)))
        }

        if let sortBy = sortBy {
            queryItems.append(sortBy.asQueryItem())
        }

        return queryItems
    }
}
