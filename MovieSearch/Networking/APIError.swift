//
//  APIError.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation

/// Enum modelling errors for APIClient
enum APIError: Error {
    case error(code: Int, message: String)
    case unknownError
}
