//
//  APIClientConfig.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation

struct APIClientConfig {
    let apiKey: String
    let baseURL: URL
    let imagesBaseURL: URL
    let discoverMoviePath: String
    let movieInfoPath: String
}
