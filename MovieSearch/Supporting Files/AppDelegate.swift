//
//  AppDelegate.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let config = APIClientConfig(
            apiKey: "328c283cd27bd1877d9080ccb1604c91",
            baseURL: URL(string: "https://api.themoviedb.org")!,
            imagesBaseURL: URL(string: "https://image.tmdb.org/t/p/w342")!,
            discoverMoviePath: "/3/discover/movie",
            movieInfoPath: "/3/movie/"
        )

        let networkClient = MovieDBAPIClient(config: config)

        var params = Params()
        params.releasedBefore = Date(timeIntervalSince1970: 1483142400)
        params.sortBy = .descending(.releaseDate)

        let dataStore = ListingDataStore(
            apiClient: networkClient,
            params: params
        )

        let navController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? UINavigationController

        if let listingVC = navController?.viewControllers.first as? ListingViewController {
            listingVC.dataStore = dataStore
        }

        window?.rootViewController = navController

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
}

