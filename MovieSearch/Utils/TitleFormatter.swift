//
//  TitleFormatter.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation

enum TitleFormatter {
    static func format(title: String, originalTitle: String) -> String {

        /// Show the title as:
        ///   title (original title) - if both are present and not equal
        ///   title                  - if only title is present
        ///   original title         - if only original title is present
        if !originalTitle.isEmpty && !title.isEmpty && title != originalTitle {
            return title + " (" + originalTitle + ")"
        } else if !title.isEmpty {
            return title
        } else if !originalTitle.isEmpty {
            return originalTitle
        }

        return ""
    }
}
