//
//  StoryboardInstantiable.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import UIKit

protocol StoryboardInstantiable {
    static var storyboardName: String { get }
    static var identifier: String { get }
    static var bundle: Bundle? { get }
}

extension StoryboardInstantiable where Self: UIViewController {
    static func instantiateFromStoryboard() -> Self {
        let storyboard = UIStoryboard(
            name: storyboardName,
            bundle: bundle
        )

        let mayBeVC = storyboard.instantiateViewController(withIdentifier: identifier)

        guard let vc = mayBeVC as? Self else {
            fatalError("Couldn't load view controller: \(self) with identifier: \(identifier) from storyboard: \(storyboardName)")
        }

        return vc
    }
}
