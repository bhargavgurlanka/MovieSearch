//
//  ConfigurableCell.swift
//  MovieSearch
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import Foundation

protocol ConfigurableCell {
    func configure(with: Any)
}
