//
//  MovieDBAPIClientTests.swift
//  MovieSearchTests
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import MovieSearch

class MovieDBAPIClientTests: XCTestCase {

    var apiClient: MovieDBAPIClient? = nil

    override func setUp() {
        let config = APIClientConfig(
            apiKey: "foo",
            baseURL: URL(string: "https://api.themoviedb.org")!,
            imagesBaseURL: URL(string: "https://image.tmdb.org/t/p/w342")!,
            discoverMoviePath: "/3/discover/movie",
            movieInfoPath: "/3/movie/"
        )

        apiClient = MovieDBAPIClient(config: config)
    }
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
    }

    func testParsingMoviesList() {
        stub(condition: isPath("/3/discover/movie")) { request in
            let stubPath = OHPathForFile("movies_list_response.json", type(of: self))
            return fixture(filePath: stubPath!, headers: ["Content-Type": "application/json"])
        }

        apiClient!.fetchMovies(params: Params(), page: 1) { result in
            switch result {
            case .failure:
                XCTFail("Network request should not fail")
            case let .success(response):
                XCTAssertEqual(response.page, 1)
                XCTAssertEqual(response.totalPages, 14755)
                XCTAssertEqual(response.totalResults, 295098)
                XCTAssertEqual(response.results.map({ $0.id }), [ 459123,
                                                                  432590,
                                                                  428142,
                                                                  466199,
                                                                  432461,
                                                                  433077,
                                                                  433597,
                                                                  430993,
                                                                  432484,
                                                                  456538,
                                                                  326035,
                                                                  447704,
                                                                  432444,
                                                                  263508,
                                                                  270776,
                                                                  350038,
                                                                  350039,
                                                                  350051,
                                                                  349204,
                                                                  364748 ])
            }
        }
    }

    func testParsingMovieInfo() {
        stub(condition: isPath("/3/movie/428142")) { request in
            let stubPath = OHPathForFile("movie_info_428142.json", type(of: self))
            return fixture(filePath: stubPath!, headers: ["Content-Type": "application/json"])
        }

        apiClient!.fetchMovieInfo(id: 428142) { result in
            switch result {
            case .failure:
                XCTFail("Network request should not fail")
            case let .success(response):
                XCTAssertEqual(response.id, 428142)
                XCTAssertEqual(response.posterURL, URL(string: "https://image.tmdb.org/t/p/w342/cxpFqh4OVMHCKniQKjAxQGDQgSm.jpg"))
                XCTAssertEqual(response.title, "Fate/Grand Order: First Order")
                XCTAssertEqual(response.originalTitle, "Fate/Grand Order: First Order")
                XCTAssertEqual(response.originalLanguage, "ja")
                XCTAssertEqual(response.runtime, 72)
                XCTAssertEqual(response.genres, ["Animation", "Action", "Drama"])
            }
        }
    }
}
