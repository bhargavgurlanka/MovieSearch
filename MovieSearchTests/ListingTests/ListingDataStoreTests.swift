//
//  ListingDataStoreTests.swift
//  MovieSearchTests
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import XCTest
import Result
@testable import MovieSearch

class ListingDataStoreTests: XCTestCase {
    private var dataStore: ListingDataStore!
    private var mockClient: MockAPIClient!
    private var mockDelegate: MockDelegate!

    override func setUp() {
        mockClient = MockAPIClient()
        dataStore = ListingDataStore(apiClient: mockClient, params: Params())
        mockDelegate = MockDelegate()
    }

    func testInitialization() {
        XCTAssertEqual(dataStore.lastPageNumber, nil)
        XCTAssertEqual(dataStore.totalPagesCount, nil)
    }
    
    func testPageSizeSetProperly() {
        /// Initial request
        do {
            mockClient.result = .success(FetchMoviesListResponse(page: 1, results: [], totalResults: 30, totalPages: 6))
            dataStore.fetchNextPage()

            XCTAssertEqual(mockClient.requestedPage, 1)
        }

        /// Next pages
        do {
            dataStore.fetchNextPage()
            XCTAssertEqual(mockClient.requestedPage, 2)
        }
    }

    func testPageNumberAfterFailure() {
        /// Initial request
        do {
            mockClient.result = .failure(.unknownError)
            dataStore.fetchNextPage()

            XCTAssertEqual(mockClient.requestedPage, 1)
        }

        /// Next pages
        do {
            dataStore.fetchNextPage()
            XCTAssertEqual(mockClient.requestedPage, 1)
        }
    }

    func testDelegatesGettingCalled() {
        dataStore.delegate = mockDelegate
        dataStore.fetchNextPage()

        XCTAssert(mockDelegate.beginFetchingCalled)
        XCTAssert(mockDelegate.endFetchingCalled)
    }

    func testAppendingRows() {
        /// First page
        mockClient.result = .success(
            FetchMoviesListResponse(
                page: 1,
                results: [
                    FetchMoviesListResponse.Movie(id: 1, posterURL: nil, title: "foo", overview: "", originalTitle: "bar", popularity: 1)
                ],
                totalResults: 2,
                totalPages: 2
            )
        )
        dataStore.fetchNextPage()

        /* Movie + loading cell */
        XCTAssertEqual(dataStore.rows.count, 2)
        XCTAssertNotNil(dataStore.rows[0] as? ListingMovieCellViewModel)
        XCTAssertNotNil(dataStore.rows[1] as? ListingLoadingCellViewModel)

        /// Second page
        mockClient.result = .success(
            FetchMoviesListResponse(
                page: 2,
                results: [
                    FetchMoviesListResponse.Movie(id: 2, posterURL: nil, title: "bar", overview: "", originalTitle: "foo", popularity: 1)
                ],
                totalResults: 2,
                totalPages: 2
            )
        )
        dataStore.fetchNextPage()

        /* Movie + Movie */
        XCTAssertEqual(dataStore.rows.count, 2)
        XCTAssertNotNil(dataStore.rows[0] as? ListingMovieCellViewModel)
        XCTAssertNotNil(dataStore.rows[1] as? ListingMovieCellViewModel)
    }

    func testFetchingDuplicatePages() {
        /// First page
        mockClient.result = .success(
            FetchMoviesListResponse(
                page: 1,
                results: [
                    FetchMoviesListResponse.Movie(id: 1, posterURL: nil, title: "foo", overview: "", originalTitle: "bar", popularity: 1)
                ],
                totalResults: 2,
                totalPages: 2
            )
        )
        dataStore.fetchNextPage()

        /* Movie + loading cell */
        XCTAssertEqual(dataStore.rows.count, 2)
        XCTAssertNotNil(dataStore.rows[0] as? ListingMovieCellViewModel)
        XCTAssertNotNil(dataStore.rows[1] as? ListingLoadingCellViewModel)

        /// Second page
        mockClient.result = .success(
            FetchMoviesListResponse(
                page: 1,
                results: [
                    FetchMoviesListResponse.Movie(id: 2, posterURL: nil, title: "bar", overview: "", originalTitle: "foo", popularity: 1)
                ],
                totalResults: 2,
                totalPages: 2
            )
        )
        dataStore.fetchNextPage()

        /* Movie + Loading cell */
        XCTAssertEqual(dataStore.rows.count, 2)
        XCTAssertNotNil(dataStore.rows[0] as? ListingMovieCellViewModel)
        XCTAssertEqual((dataStore.rows[0] as? ListingMovieCellViewModel)?.id, 1)
        XCTAssertNotNil(dataStore.rows[1] as? ListingLoadingCellViewModel)
    }

    func testReset() {
        /// First page
        mockClient.result = .success(
            FetchMoviesListResponse(
                page: 1,
                results: [
                    FetchMoviesListResponse.Movie(id: 1, posterURL: nil, title: "foo", overview: "", originalTitle: "bar", popularity: 1)
                ],
                totalResults: 2,
                totalPages: 2
            )
        )
        dataStore.fetchNextPage()

        /* Movie + loading cell */
        XCTAssertEqual(dataStore.rows.count, 2)
        XCTAssertNotNil(dataStore.rows[0] as? ListingMovieCellViewModel)
        XCTAssertNotNil(dataStore.rows[1] as? ListingLoadingCellViewModel)

        dataStore.reset()

        /// Second page
        mockClient.result = .success(
            FetchMoviesListResponse(
                page: 1,
                results: [
                    FetchMoviesListResponse.Movie(id: 2, posterURL: nil, title: "bar", overview: "", originalTitle: "foo", popularity: 1)
                ],
                totalResults: 2,
                totalPages: 2
            )
        )
        dataStore.fetchNextPage()

        /* Movie + Loading cell */
        XCTAssertEqual(dataStore.rows.count, 2)
        XCTAssertNotNil(dataStore.rows[0] as? ListingMovieCellViewModel)
        XCTAssertEqual((dataStore.rows[0] as? ListingMovieCellViewModel)?.id, 2)
        XCTAssertNotNil(dataStore.rows[1] as? ListingLoadingCellViewModel)
    }
}


//MARK:- Mocks
fileprivate class MockAPIClient: APIClient {
    var params: Params? = nil
    var requestedPage: Int? = nil
    var result: Result<FetchMoviesListResponse, APIError> = .failure(.unknownError)

    func fetchMovies(params: Params, page: Int, completion: @escaping ((Result<FetchMoviesListResponse, APIError>) -> Void)) {
        self.requestedPage = page
        self.params = params

        completion(result)
    }

    func fetchMovieInfo(id: Int, completion: @escaping ((Result<FetchMovieInfoResponse, APIError>) -> Void)) {
    }
}

fileprivate class MockDelegate: ListingDataStoreDelegate {
    private(set) var beginFetchingCalled = false
    private(set) var endFetchingCalled = false

    func didBeginFetching() {
        beginFetchingCalled = true
    }

    func didEndFetching() {
        endFetchingCalled = true
    }
}
