//
//  ListingMovieCellViewModelTests.swift
//  MovieSearchTests
//
//  Created by Gurlanka, Bhargav on 08/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import XCTest
import Result
@testable import MovieSearch

class ListingMovieCellViewModelTests: XCTestCase {
    func testFormattingWithTitleOriginalTitle() {
        let movie = FetchMoviesListResponse.Movie(id: 1, posterURL: nil, title: "foo", overview: "", originalTitle: "bar", popularity: 1)
        let viewModel = ListingMovieCellViewModel(movie)
        
        XCTAssertEqual(viewModel.title, "foo (bar)")
    }

    func testFormattingWithSameTitleOriginalTitle() {
        let movie = FetchMoviesListResponse.Movie(id: 1, posterURL: nil, title: "foo", overview: "", originalTitle: "foo", popularity: 1)
        let viewModel = ListingMovieCellViewModel(movie)

        XCTAssertEqual(viewModel.title, "foo")
    }

    func testFormattingWithOnlyTitle() {
        let movie = FetchMoviesListResponse.Movie(id: 1, posterURL: nil, title: "foo", overview: "", originalTitle: "", popularity: 1)
        let viewModel = ListingMovieCellViewModel(movie)

        XCTAssertEqual(viewModel.title, "foo")
    }

    func testFormattingWithOnlyOriginalTitle() {
        let movie = FetchMoviesListResponse.Movie(id: 1, posterURL: nil, title: "", overview: "", originalTitle: "bar", popularity: 1)
        let viewModel = ListingMovieCellViewModel(movie)

        XCTAssertEqual(viewModel.title, "bar")
    }

}
