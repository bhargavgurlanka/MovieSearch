//
//  ParamsTests.swift
//  MovieSearchTests
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import XCTest
@testable import MovieSearch

class ParamsTests: XCTestCase {
    func testDateFormat() {
        var params = Params()
        params.releasedBefore = Date(timeIntervalSince1970: 1507542262)

        XCTAssertEqual(params.asQueryItems().count, 1)
        XCTAssertEqual(params.asQueryItems()[0], URLQueryItem(name: "primary_release_date.lte", value: "2017-10-09"))
    }

    func testSortAscending() {
        var params = Params()
        params.sortBy = .ascending(.releaseDate)

        XCTAssertEqual(params.asQueryItems().count, 1)
        XCTAssertEqual(params.asQueryItems()[0], URLQueryItem(name: "sort_by", value: "release_date.asc"))
    }

    func testSortDescending() {
        var params = Params()
        params.sortBy = .descending(.releaseDate)

        XCTAssertEqual(params.asQueryItems().count, 1)
        XCTAssertEqual(params.asQueryItems()[0], URLQueryItem(name: "sort_by", value: "release_date.desc"))
    }

    func testAllParamsTogether() {
        var params = Params()
        params.releasedBefore = Date(timeIntervalSince1970: 1507542262)
        params.sortBy = .descending(.releaseDate)

        XCTAssertEqual(params.asQueryItems().count, 2)
        XCTAssertEqual(params.asQueryItems()[0], URLQueryItem(name: "primary_release_date.lte", value: "2017-10-09"))
        XCTAssertEqual(params.asQueryItems()[1], URLQueryItem(name: "sort_by", value: "release_date.desc"))
    }
}
