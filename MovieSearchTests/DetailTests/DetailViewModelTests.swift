//
//  DetailViewModelTests.swift
//  MovieSearchTests
//
//  Created by Gurlanka, Bhargav on 09/10/17.
//  Copyright © 2017 Bhargav Gurlanka. All rights reserved.
//

import XCTest
@testable import MovieSearch

class DetailViewModelTests: XCTestCase {
    func testTitleFormattingWhenTitleOriginalTitlePresent() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "foo",
            originalTitle: "bar",
            overview: "",
            genres: [],
            runtime: nil,
            language: "en"
        )

        XCTAssertEqual(viewModel.title, "foo (bar)")
    }

    func testTitleFormattingWhenTitlePresent() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "foo",
            originalTitle: "",
            overview: "",
            genres: [],
            runtime: nil,
            language: "en"
        )

        XCTAssertEqual(viewModel.title, "foo")
    }

    func testTitleFormattingWhenOriginalTitlePresent() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "",
            originalTitle: "bar",
            overview: "",
            genres: [],
            runtime: nil,
            language: "en"
        )

        XCTAssertEqual(viewModel.title, "bar")
    }

    func testOverviewFormatting() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "",
            originalTitle: "bar",
            overview: "This is overview",
            genres: [],
            runtime: nil,
            language: "en"
        )

        XCTAssertEqual(viewModel.overview, "Overview: This is overview")
    }

    func testNoOverviewFormatting() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "",
            originalTitle: "bar",
            overview: "",
            genres: [],
            runtime: nil,
            language: "en"
        )

        XCTAssertEqual(viewModel.overview, "Overview: -")
    }

    func testGenresFormatting() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "",
            originalTitle: "bar",
            overview: "",
            genres: ["Animation", "Horror"],
            runtime: nil,
            language: "en"
        )

        XCTAssertEqual(viewModel.genres, "Genres: Animation, Horror")
    }

    func testNoGenresFormatting() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "",
            originalTitle: "bar",
            overview: "",
            genres: [],
            runtime: nil,
            language: "en"
        )

        XCTAssertEqual(viewModel.genres, "Genres: -")
    }

    func testRuntimeFormatting() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "",
            originalTitle: "bar",
            overview: "",
            genres: [],
            runtime: 72,
            language: "en"
        )

        XCTAssertEqual(viewModel.runtime, "Runtime: 72 minutes")
    }

    func testNoRuntimeFormatting() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "",
            originalTitle: "bar",
            overview: "",
            genres: [],
            runtime: nil,
            language: "en"
        )

        XCTAssertEqual(viewModel.runtime, "Runtime: -")
    }

    func testLanguageFormatting() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "",
            originalTitle: "bar",
            overview: "",
            genres: [],
            runtime: nil,
            language: "en"
        )

        XCTAssertEqual(viewModel.language, "Language: en")
    }

    func testNoLanguageFormatting() {
        let viewModel = DetailViewModel(
            id: 1,
            posterURL: nil,
            title: "",
            originalTitle: "bar",
            overview: "",
            genres: [],
            runtime: nil,
            language: ""
        )

        XCTAssertEqual(viewModel.language, "Language: -")
    }
}
